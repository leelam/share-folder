const axios = require('axios');
const elasticsearch = require('elasticsearch');
const _ = require('lodash');
const https = require('https');

// At request level
const agent = new https.Agent({
    rejectUnauthorized: false
});


const client = new elasticsearch.Client({
    host: 'localhost:9200'
});
const lat = [10.76242, 10.89820];
const lng = [106.80000, 106.88003]

const str = "</va></mo><mo id='C2'><fi>";
const to = "Tờ bản đồ:</fi><va>";
let number = 1000000000;

const crawlData = async (lng, lat) => {
    try {
        let esData = await client.search({
            index: 'crawldata',
            body: {
                "query": {
                    "filtered": {
                        "filter": {
                            "geo_shape": {
                                "location": {
                                    "relation": "intersects",
                                    "shape": {
                                        "coordinates": [
                                            lng,
                                            lat
                                        ],
                                        "type": "point"
                                    }
                                }
                            }
                        },
                        "query": {
                            "match_all": {}
                        }
                    }
                }
            }
        })
    
        esData = _.get(esData,'hits.total');
        console.log(esData);
        if(esData) {
            return true
        }  else {
            const url = `https://quyhoach-quan9.hochiminhcity.gov.vn/api/GIS/Mode=Select2,${lng},${lat}/`;
            let jsonData = await getttt(url);
            const dataCrawl = _.get(jsonData, 'data')
            if(!dataCrawl || dataCrawl === 'Không xác định thửa đất tại vị trí này.' || dataCrawl.indexOf('Item has already been added') !== -1) {
                return;
            };
            let data = JSON.parse(dataCrawl);
            data = _.get(data, 'features')
            number = number + 1;
            await data.map(async item => {
                const properties = await getData(item);
                await client.index({
                    index: 'crawldata',
                    type: 'doc',
                    body: {
                        number_check: number,
                        properties: properties,
                        location: item.geometry
                    }
                })
            })
            console.log('done:', number);
            return true
        };    
    } catch (error) {
        console.log("Có lỗi:" + error.toString(), '--------------------------------------------------------',lng,lat, number);
        process.exit()
    }
   
};

const getttt = async (url) => {
    try {
        const jsonData = await axios.get(url, { httpsAgent: agent });
        return jsonData;
    } catch (error) {
        console.log(error.toString(), url);
        return [];
    }
}

const getData = async (data) => {
    let properties = _.get(data, 'properties.oContent');
    const arr = properties.split(str);
    properties = await getProperties(arr);
    const pro = {}
    for (i = 0; i < properties.length; i++) {
        if (!([5, 22].includes(i))) {
            pro[properties[i][0]] = properties[i][1]
        }
    }
    return pro
}

const getProperties = async (arr) => {
    let arrProperties = [];
    await arr.map(item => {
        let pro = [];
        const indexOfTo = item.indexOf(to);
        if (indexOfTo !== -1) {
            pro[0] = "Tờ bản đồ";
            pro[1] = item.slice(indexOfTo + to.length)
        } else {
            pro = item.split("</fi><va>")
        }
        arrProperties.push(pro)
    })
    return arrProperties
}

const main = async () => {
    let i = lng[0];
   
    do {
        let j = lat[0];
        do {
            await crawlData(i,j);
            j += 0.00002
        } while (j < lat[1])
        i += 0.00002
    } while (i < lng[1])
}
main()
// crawlData(106.7865453734953,10.832642242306292)
