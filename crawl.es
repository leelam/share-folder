 DELETE /crawldata

DELETE /crawldata

GET /crawldata/_search
{
  "query": {
    "filtered": {
      "filter": {
        "geo_shape": {
          "location": {
            "relation": "intersects",
            "shape": {
              "coordinates": [
               106.759471957847, 10.817634355217
              ],
              "type": "point"
            }
          }
        }
      },
      "query": {
        "match_all": {}
      }
    }
  }
}

GET /crawldata/_search
{
    "query": {
        "bool" : {
            "must" : {
                "match_all" : {}
            },
            "filter" : {
                "geo_polygon" : {
                    "location" : {
                        "points" : [
                            106.789307128188, 10.7979946550764
                        ]
                    }
                }
            }
        }
    }
}

PUT /crawldata
{
    "mappings": {
        "doc": {
            "properties": {
                "location": {
                    "type": "geo_shape"
                },
                "number_check"  : { "type" : "long"    }
            }
        }
    }
}
GET /crawldata/_search
{
  "sort": [
    {
      "number_check": {"order":"desc"}
    }
  ]
}

POST /crawldata/_close

PUT /crawldata/_settings
{ 
  "max_result_window" : 2147483647  
}

POST /crawldata/_open

DELETE /crawldata
